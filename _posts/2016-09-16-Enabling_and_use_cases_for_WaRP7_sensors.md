---
title: Enabling and use cases for WaRP7 sensors
redirect_from: blog/tutorial/2016/09/16/WaRP7_Sensors/
date: 2016-09-16 10:33:32
author: breno
tags: [imx7s, warp, warp7, sensors, FXOS8700, FXAS21002, MPL3115A2]
categories: [Tutorial]
---

WaRP7 daughter card includes 3 NXP sensors:

* FXOS8700 -> Accelerometer + Magnetometer
* FXAS21002 -> Gyroscope
* MPL3115A2 -> Pressure sensor + thermometer

Each sensor is controlled through I2C. The sensors data can be read by the
following methods.

{: .notice--warning}
This procedure is just possible when using Linux Kernel v4.1.32 or higher.
In order to update the kernel you can clone and compile it
([Link to linux-fslc][1]) or burn the latest WaRP7 image from FSL Community BSP
([Link to download][2]).
All the procedure to achieve the methods above can be found in the WaRP7 User
Guide ([Link to WaRP7 User Guide][3]).

[1]:https://github.com/WaRP7/linux-fslc
[2]:http://freescale.github.io/#download
[3]:https://github.com/WaRP7/WaRP7-User-Guide/releases

## Defining the sensors:
First of all it's necessary to define which driver are you going to use
in kernel, edit your defconfig file as following:
```
Defining FXOS8700:
CONFIG_SENSORS_FXOS8700=y

Defining FXAS21002:
CONFIG_SENSORS_FXAS2100X=y

Defining MPL3115:
Currently there are two driver implementations for the MPL3115.
Kernel mainline driver:
CONFIG_MPL3115=y
NXP driver:
CONFIG_INPUT_MPL3115=y
```

Once your board is running it's necessary to enable the sensors:
```
Enabling FXOS8700:
$ echo 1 > /sys/devices/virtual/misc/FreescaleAccelerometer/enable
$ echo 1 > /sys/devices/virtual/misc/FreescaleMagnetometer/enable

Enabling FXAS21002:
$ echo 1 > /sys/devices/virtual/misc/FreescaleGyroscope/enable

Enabling MPL3115A2:
$ echo 1 > /sys/devices/virtual/input/input3/enable
Note: MPL3115A2 is already enabled by default when using kernel mainline
driver.
```

## Test sensors with evtest

Evtest can be used for reading the sensors. Note that MPL3115 it's not
available in evtest when using kernel mainline driver:
```
$ evtest

No device specified, trying to scan all of /dev/input/event*
Available devices:
/dev/input/event0:      fxos8700
/dev/input/event1:      fxas2100x
/dev/input/event2:      30370000.snvs:snvs-powerkey
/dev/input/event3:      gpio-keys
Select the device event number [0-3]:
```

Select the desired sensor and the data is outputted:
```
Select the device event number [0-3]: 0
Input driver version is 1.0.1
Input device ID: bus 0x18 vendor 0x0 product 0x0 version 0x0
Input device name: "fxos8700"
Supported events:
  Event type 0 (EV_SYN)
  Event type 3 (EV_ABS)
    Event code 0 (ABS_X)
      Value   -952
      Min     -511
      Max      511
    Event code 1 (ABS_Y)
      Value    -52
      Min     -511
      Max      511
    Event code 2 (ABS_Z)
      Value  16256
      Min     -511
      Max      511
Properties:
Testing ... (interrupt to exit)
Event: time 1474054151.543933, type 3 (EV_ABS), code 0 (ABS_X), value -992
Event: time 1474054151.543933, type 3 (EV_ABS), code 1 (ABS_Y), value -32
Event: time 1474054151.543933, type 3 (EV_ABS), code 2 (ABS_Z), value 16244
Event: time 1474054151.543933, -------------- SYN_REPORT ------------
```

For users using the kernel mainline the pressure can be read in the file
`in_pressure_raw`:
```
$ cat /sys/bus/iio/devices/iio:device0/in_pressure_raw
378282
```
# Read sensors data through i2c-tools

As an alternative to evtest, i2c-tools can be used to read the sensors
registers.
This method should not be used as an official method. Once the sensor is in use
by the kernel the register is not accessible through i2c-tools, in order to
read the sensors registers is necessary to force the output, please use this
method just for testing purpose.

## List Sensors:

Listing all I2C buses:
```
$ i2cdetect -l

i2c-0   i2c             30a20000.i2c                            I2C adapter
i2c-1   i2c             30a30000.i2c                            I2C adapter
i2c-2   i2c             30a40000.i2c                            I2C adapter
i2c-3   i2c             30a50000.i2c                            I2C adapter
```
On WaRP7 all sensors are located on I2C4, in order to list all the sensors
addresses use the command below:

```
$ i2cdetect -y -q 3

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- UU -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- UU -- 
20: UU -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: UU -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- -- 

NOTE: UU means that the device or resource is busy. This means that the device
is already being use by the kernel.
```

## I2C addresses

The sensors are located in the following addresses:
* FXOS8700 -> 0x1e
* FXAS21002 -> 0x20
* MPL3115A2 -> 0x60

All sensors registers can be read by running the i2cdump command, specifying
the sensor address:

```
$i2cdump -f -y 3 0x1e

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f    0123456789abcdef
00: ff 03 94 ff f0 3e d8 00 00 00 00 01 00 c7 00 00    .??.?>?....?.?..
10: 00 80 00 84 44 00 00 00 00 00 00 00 00 00 00 00    .?.?D...........
20: 00 00 00 00 00 00 00 00 00 00 19 00 00 00 00 00    ..........?.....
30: 00 00 ff fd ca 00 cf 00 39 00 00 00 00 00 00 00    ...??.?.9.......
40: 00 00 00 00 00 80 00 80 00 80 00 7f ff 7f ff 7f    .....?.?.?.?.?.?
50: ff 24 00 00 00 00 00 00 00 00 00 1f 58 00 00 00    .$.........?X...
60: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
70: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
80: 38 c7 c6 6c 60 0c 0b 00 00 74 a7 fc 02 11 42 00    8??l`??..t????B.
90: 04 22 1d fb f9 14 f7 00 00 00 25 dc 23 50 26 a2    ?"?????...%?#P&?
a0: 72 90 00 00 b6 cc aa 90 00 00 00 00 00 00 00 00    r?..????........
b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
c0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
d0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
e0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................
f0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ................

```
## Read registers

In order to read a single register, the i2cget command can be used.
For instance you can read the FXOS8700 device ID in the following way:
```
$ i2cget -f -y 3 0x1e 0x0D
Returns: 0xc7
```

All the sensor and register descriptions can be obtained in the sensors
datasheet, please find below the download link:

* FXOS8700: [Download link][1]
* FXAS21002: [Download link][2]
* MPL3115A2: [Download link][3]

[1]:http://cache.nxp.com/files/sensors/doc/data_sheet/FXOS8700CQ.pdf?pspll=1
[2]:http://cache.nxp.com/files/sensors/doc/data_sheet/FXAS21002.pdf?pspll=1
[3]:http://cache.nxp.com/files/sensors/doc/data_sheet/MPL3115A2.pdf?pspll=1

## Unit test

There is a set of scripts that can be used alternatively to the procedure
described on this post which can be  downloaded and cloned from the following
repository:

[Warp 7 Unit Test][4] 

[4]: https://github.com/WaRP7/warp7-unit-test

