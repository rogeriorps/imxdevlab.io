---
title: Avoiding binder issue on android pre-built image
date: 2016-10-17 10:43:42
author: breno
categories: [ Tutorial ]
tags: [ binder, m6.0.1_2.1.0-ga, marshmallow, error, android, issue, imx, imx6, boot ]
---

When working with pre-built Android images you may face the binder issue, this
post shows a workaround to avoid this error by changing to a non-secure boot.
It was tested on i.MX6Q and i.MX6DL, but the same can be applied to other
boards.

For example the log error of a i.MX6Q running Android 6.0.1 marshmallow:

```
init: (Loading properties from /system/build.prop took 0.00s.)
init: (Loading properties from /vendor/build.prop took 0.00s.)
init: (Loading properties from /factory/factory.prop took 0.00s.)
init: cannot find '/system/bin/logd', disabling 'logd'
init: cannot find '/system/bin/vold', disabling 'vold'
init: Not bootcharting.
init: cannot find '/system/bin/tzdatacheck', disabling 'exec 0 (/system/bin/tzdatacheck)'
random: init urandom read with 108 bits of entropy available
random: nonblocking pool is initialized
init: cannot find '/system/bin/logd', disabling 'logd'
init: cannot find '/system/bin/logd', disabling 'logd-reinit'
init: Starting service 'healthd'...
init: cannot find '/system/bin/sh', disabling 'console'
init: cannot find '/system/bin/lmkd', disabling 'lmkd'
binder: 210:210 transaction failed 29189, size 0-0
init: cannot find '/system/bin/servicemanager', disabling 'servicemanager'
init: cannot find '/system/bin/surfaceflinger', disabling 'surfaceflinger'
init: write_file: Unable to open '/sys/block/mmcblk0/queue/read_ahead_kb': No such file or directory
init: write_file: Unable to open '/sys/block/mmcblk1/queue/read_ahead_kb': No such file or directory
rfkill: BT RF going to : off
init: property_set("ro.ril.wake_lock_timeout", "300") failed
init: property 'ro.product.manufacturer' doesn't exist while expanding '${ro.product.manufacturer}'
init: cannot expand '${ro.product.manufacturer}' while writing to '/config/usb_gadget/g1/strings/0x409/manufacturer'
init: property 'ro.product.model' doesn't exist while expanding '${ro.product.model}'
init: cannot expand '${ro.product.model}' while writing to '/config/usb_gadget/g1/strings/0x409/product'
init: Starting service 'adbd'...
read descriptors
read strings
binder: 210:210 transaction failed 29189, size 0-0
binder: 210:210 transaction failed 29189, size 0-0
binder: 210:210 transaction failed 29189, size 0-0
binder: 210:210 transaction failed 29189, size 0-0
binder: 210:210 transaction failed 29189, size 0-0
binder: 210:210 transaction failed 29189, size 0-0
binder: 210:210 transaction failed 29189, size 0-0
```

To avoid this error you may add the variable androidboot.dm_verity on your
bootargs:

```
$ setenv bootargs YOUR_BOOT_ARGS androidboot.dm_verity=disabled
```

When this variable is disabled the integrity checking on the system partition is
bypassed, this is usually set when the binary in the system partition needs
to be changed during the development.

If you want to know more about verified boot, please visit the following link:

<https://source.android.com/security/verifiedboot/index.html>
