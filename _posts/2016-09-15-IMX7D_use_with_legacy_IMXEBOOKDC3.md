---
title: IMX7D configuration with legacy IMXEBOOKDC3
redirect_from: blog/tutorial/2016/09/15/IMX7D_use_with_legacy_IMXEBOOKDC3/
date: 2016-09-15 17:19:23
author: andres
categories: [Tutorial] 
tags: [DC3, IMX7D, EPDC, E-ink, eink, IMXEBOOKDC3] 
---

The i.MX7D SDB board can be purchased from NXP with a IMXEBOOKDC4 expansion board for E-ink prototyping.

There is this useful post on the NXP community that illustrates how to connect the IMXEBOOKDC4 to an i.MX7D SDB board. 

<https://community.nxp.com/docs/DOC-328663>

I got an i.MX7D but I could only source a IMXEBOOKDC3. The IMXEBOOKDC3 was launched in conjunction with the i.MX6SL which was the previous generation of NXP boards with the Electronic Paper Display Controller (EPDC). I tried to follow this steps on the i.MX7D+IMXEBOOKDC4 post but unfortunately couldn't make the IMXEBOOKDC3 work. 

After some digging I managed to use i.MX7D with the IMXEBOOKDC3 but some considerations have to be taken into account:

1: The IMXEBOOKDC3 has to be connected through the top connector rather than the side connector as shown on the IMXEBOOKDC4 example. The images below show the differences in connecting the boards:
 
<img src="{{ site.url }}{{ site.baseurl }}/images/IMX7D_DC3.jpg">

<img src="{{ site.url }}{{ site.baseurl }}/images/IMX7D_DC4.jpg">

2: Some of the tutorials online including the original post for the IMXEBOOKDC4 instruct the user to include the command:

```console
=> setenv mmcargs 'setenv bootargs console=${console},${baudrate} root=${mmcroot} epdc video=mxcepdcfb:E060SCM,bpp=16'
```

This command is **not** needed on the case of the IMX7D+IMXEBOOKDC3. The default mmcargs should be fine for IMXEBOOKDC3. An example of the default mmcargs on my image can be seen below:

```console
mmcargs=setenv bootargs console=ttymxc0,115200 root=/dev/mmcblk0p2 rootwait rw
```

3: The default dtb needs to be replaced by an EPDC specific dtb. The i.MX7D 4.1.15 Linux Kernel image that NXP distributes on its website comes with the EPDC dtb. You can check the EPDC dtb on this link <http://git.freescale.com/git/cgit.cgi/imx/linux-2.6-imx.git/tree/arch/arm/boot/dts/imx7d-sdb-epdc.dts?h=imx_4.1.15_1.0.0_ga>. Run the following command on the U-Boot prompt to switch the default dtb:

```console
=> setenv fdt_file imx7d-sdb-epdc.dtb
```

4: You can now boot the board. In order to test, run the command below from the Linux command line to test your setup:

```console
$ ./unit_tests/mxc_epdc_v2_fb_test.out
```

