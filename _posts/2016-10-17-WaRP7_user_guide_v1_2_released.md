---
title: WaRP7 user guide v1_2 released
date: 2016-10-17 15:32:57
author: andres
tags: [ warp7, IMX7S, Documentation ]
category: [ Announcement ]
---

A new release of the WaRP7 user guide has been made available on the WaRP7 GitHub repository.

<https://github.com/WaRP7/WaRP7-User-Guide/releases/tag/v1.2>

This new release contains fixes to the chapter numbering an a new section called ["Use Cases"](https://github.com/WaRP7/WaRP7-User-Guide/blob/master/07-Chapter/Use_Cases.adoc). For now, this chapter only includes a Wi-Fi enablement section but the expectation is for it to be filled with peripheral implementations from the WaRP7 team and users. 

The WaRP7 documentation source code is open for the public to propose changes and additions. To do so the user needs to send pull request with the changes which then gets reviewed and potentially approved by the WaRP7 team.


