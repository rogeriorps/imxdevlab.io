---
title: How to use HDMI output on iMX 6SoloX SABRE-SD
date: 2016-11-24 12:53:27
author: marco
categories: [ Tutorial ]
tags: [ u-boot, gstreamer, HDMI, IMX6SX, mcimxhdmicard ]
---
The i.MX 6SoloX SABRE-SD supports the parallel Seiko WVGA LCD by default. However, there is also a HDMI Port Card expander board that provides HDMI output. This HDMI expander board connects in the same LCD parallel bus as the Seiko WVGA display.
<figure>
<img src="{{ base_path }}/images/hdmiexpander.png" alt="HDMI Port Card MCIMXHDMI CARD" align="middle" width="600"/>
    <figcaption align="middle">HDMI Port Card MCIMXHDMICARD</figcaption>
</figure>
In order to use the parallel LCD output for this purpose, it's necessary to set up the environment variables below on U-Boot:
```
=> setenv fdt_file ‘imx6sx-sdb-lcdif1.dtb’
=> setenv mmcargs ‘setenv bootargs console=${console},${baudrate} root=${mmcroot} video=mxc_lcdif:SEIKO-WVGA, bpp=16’
=> saveenv
```
With these configurations, the board is able to run videos using the standard LCD SEIKO WVGA panel or the HDMI Port Card MCIMXHDMICARD expander board which share the same connector.
