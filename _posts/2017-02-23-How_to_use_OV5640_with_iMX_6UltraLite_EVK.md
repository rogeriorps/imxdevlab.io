---
title: How to use OV5640 with iMX 6UltraLite EVK
date: 2017-02-23 16:11:35
author: marco
categories: [ Tutorial ]
tags: [ 4.1.15-2.0.0_GA, gstreamer, camera, IMX6UL, ov5640 ]
---
The i.MX 6UltraLite EVK kernel release includes the parallel camera OV5640 by default. However, this camera requires a 700-27820 adapter:
<figure>
<img src="{{ base_path }}/images/camera_adapter.png" alt="Camera Adapter" align="middle" width="300"/>
    <figcaption align="middle">Camera Adapter</figcaption>
</figure>
Plug in the camera with the adapter and connect the other end of the adapter to the board:
<figure>
<img src="{{ base_path }}/images/camera_position.png" alt="Camera position" align="middle" width="300"/>
    <figcaption align="middle">Camera position</figcaption>
</figure>
In order to use the parallel OV5640 camera, it's necessary to set up the environment variable below on U-Boot:

```
=> setenv fdt_file ‘imx6ul-14x14-evk-csi.dtb’
=> saveenv
```
Follow the GStreamer pipeline example to test the camera connection:
```
$ gst-launch-1.0 v4l2src device=/dev/video1 ! video/x-raw,width=640,height=480 ! autovideosink
```
<figure>
<img src="{{ base_path }}/images/camera_test.png" alt="Camera test" align="middle" width="300"/>
    <figcaption align="middle">Camera test</figcaption>
</figure>
This test was done using the kernel BSP release 4.1.15-2.0.0 GA.
