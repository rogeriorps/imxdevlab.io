---
title: How to build Yocto SDK and compile for i.MX
date: 2017-05-16 16:39:08
author: Rogerio Pimentel da Silva
categories: [ Tutorial ]
tags: [ yocto, build, compile, sdk, crosscompile ]
---
{% include base_path %}
The first step is to create the Yocto SDK. After building an image (e.g. bitbake fsl-image-gui),  build the Yocto SDK using the command:

    $bitbake <image name\> -c populate_sdk

Example: 

    $bitbake fsl-image-gui -c populate_sdk

Bitbake will build all packages to create the sdk and also the sdk installation script. This script will be located at:

    <build_folder>/tmp/deploy/sdk/<script_name>.sh

Execute this script **Without adding 'source' or '. ./' before the script**. Just execute like in this example:

    $cd tmp/deploy/sdk/
    $./fsl-imx-fb-glibc-x86_64-fsl-image-gui-cortexa9hf-neon-toolchain-4.1.15-2.0.1.sh

The following message will be displayed asking the installation path to install the SDK. If you agree with the path, just press enter:

    Enter target directory for SDK (default: /opt/fsl-imx-fb/4.1.15-2.0.1): 
    You are about to install the SDK to "/opt/fsl-imx-fb/4.1.15-2.0.1". Proceed[Y/n]?

    Extracting SDK............................done
    Setting it up...done
    SDK has been successfully set up and is ready to be used.

Now SDK is already installed. Just execute the environment-setup script using 'source':

    $source /opt/fsl-imx-fb/4.1.15-2.0.1/environment-setup-cortexa9hf-neon-poky-linux-gnueabi**

All environment variable to build new programs for i.MX will be exported. Just use make to build for ARM. 

